extends Area2D

const HitEffect = preload("res://Effects/HitEffect.tscn")

signal hurtanimation_finished

func create_hit_effect(sprite_position, direction_factor):
	var effect = HitEffect.instance()
	var main = get_tree().current_scene
	main.add_child(effect)
	
	var new_x = global_position.x + sprite_position.x * direction_factor
	var position = global_position
	position.x = new_x
	
	effect.global_position = position
	effect.connect("animation_finished", self, "hit_animation_finished")

func hit_animation_finished():
	emit_signal("hurtanimation_finished")
