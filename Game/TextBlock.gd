extends Node2D

export(String, MULTILINE) var text_string = ""
export(Vector2) var box_dimension = Vector2(180, 80)

onready var text_box = $TextBox
onready var text_content = $TextBox/HBoxContainer/TextureRect3/Label

func _ready():
	text_content.text = text_string
	text_box.rect_size = box_dimension

func _on_ContactArea_body_entered(_body):
	print(_body.name)
	text_box.visible = true

func _on_ContactArea_body_exited(_body):
	print(_body.name)
	text_box.visible = false
