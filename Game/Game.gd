extends Node2D

var item_dynamite = preload("res://Ressources/Items/Dynamite.tscn")
var item_bear_trap = preload("res://Ressources/Items/BaerTrap.tscn")

var current_countdown = 3
var game_started = false

onready var player = $Player
onready var camera = $Camera2D
onready var countdown_timer = $CountdownTimer
onready var no_ammo_timer = $NoAmmoTimer
onready var countdown_ui = $CanvasLayer/Countdown
onready var countdown_label = $CanvasLayer/Countdown/CenterContainer/VBoxContainer/Number
onready var game_over_label = $CanvasLayer/GameOver/CenterContainer/GameOver
onready var battle_lost_label = $CanvasLayer/GameOver/CenterContainer/BattleLost
onready var no_ammo_label = $CanvasLayer/GameOver/CenterContainer/NoAmmo
onready var game_victory = $CanvasLayer/Victory/CenterContainer/Label
onready var map_label = $CanvasLayer/Countdown/CenterContainer/VBoxContainer/MapName
onready var animation_player_fader = $CanvasLayer/AnimationPlayerFader
onready var sound_game_over = $AudioGameOver
onready var sound_level_complete = $AudioLevelComplete
onready var sound_exit_visible = $AudioExitVisible
onready var level_music = $AudioLevelMusic
onready var pause_menu = $CanvasLayer/PauseMenu

func _ready():
	Global.player = player
	Global.reset_player_items()
	animation_player_fader.connect("animation_finished", self, "_on_animationPlayerFader_animation_finished")
	countdown_timer.connect("timeout", self, "_on_CountdownTimer_timeout")
	no_ammo_timer.connect("timeout", self, "_on_no_ammo_time_timeout")

	var map_number = get_map_number_from_dificulty(Global.difficulty)

	if Global.current_map > 0:
		map_number = Global.current_map

	if Global.is_preselected_map():
		map_number = Global.preselected_map

	if Global.debug_hints.test_map:
		map_number = 90

	Global.update_player_items_in_ui()

	setup_map(map_number)

func setup_map(map: int):
	Global.current_map = map
	var map_name = get_current_map_name()
	var map_scene = load("res://Maps/" + map_name + ".tscn")
	var map_node: Node2D = map_scene.instance()
	setup_environment(map_node)
	map_node.connect("map_finished", self, "change_map")
	map_node.connect("exit_is_visible", self, "exit_visible")
	map_node.connect("map_unresolvable", self, "check_level_state")
	map_node.setup_map(Global.difficulty)

	remove_existing_map_nodes()

	add_child(map_node)
	map_node.pause_mode = Node.PAUSE_MODE_STOP
	map_node.get_tree().paused = true

	animation_player_fader.play("fade_in")
	player.set_state_static()
	if map < 20 and Global.game_data.max_map < map:
		Global.game_data.max_map = map
		Global.save_game_data()

func get_map_number_from_dificulty(difficulty: int):
	match difficulty:
		0:
			return 1
		2:
			return 1
		_:
			return 4

func remove_existing_map_nodes():
	var nodes = get_children()
	for node in nodes:
		if node.name.begins_with("Map_"):
			node.disconnect("map_finished", self, "change_map")
			node.disconnect("exit_is_visible", self, "exit_visible")
			remove_child(node)

func setup_environment(settings: Node2D):
	camera.limit_right = settings.get_camera_limit_right()
	camera.limit_bottom = settings.get_camera_limit_bottom()
	player.position = settings.get_player_position()
	map_label.text = str(settings.map_name)

func get_current_map_name():
	return "Map_" + str(Global.current_map)

func get_current_map_node():
	return get_node(get_current_map_name())

func change_map():
	sound_level_complete.play()
	player.set_state_static()
	game_started = false
	if Global.current_map + 1 > Global.map_count or Global.is_preselected_map():
		game_victory.visible = true
		countdown_timer.wait_time = 4
		countdown_timer.start()
		Global.current_map = -1
		Global.preselected_map = -1
	else:
		animation_player_fader.play("fade_out_next_level")

func start_countdown():
	get_current_map_node().get_tree().paused = true
	countdown_ui.visible = true
	current_countdown = 3
	countdown_label.text = str(current_countdown)
	countdown_timer.one_shot = false
	countdown_timer.start()
	level_music.play()

func _on_Player_player_died():
	game_started = false
	Global.player_current_lifes = Global.player_current_lifes - 1

	if Global.player_current_lifes == 0:
		Global.player = null

	get_current_map_node().get_tree().paused = true
	player.set_state_static()
	sound_game_over.play()
	animation_player_fader.play("fade_out_player_died")

func _on_Player_player_dropped_item(item: String):
	if Global.has_player_item(item): # or true:
		var direction = player.velocity.normalized() * Vector2(-1, -1)
		var correction = Vector2(-12, -15)
		var check_correction = Vector2(5, 5) # Why the hell I have to do this too???
		var item_position = player.global_position + correction + (direction * Vector2(45, 45))
		var object_check_position = item_position + check_correction

		if not is_object_on_position(object_check_position):
			var item_instance = null
			match item:
				"dynamite":
					item_instance = item_dynamite.instance()
				"bear_trap":
					item_instance = item_bear_trap.instance()

			item_instance.global_position = item_position
			# item_instance.get_node("Hitbox/CollisionShape2D").disabled = true
			get_current_map_node().get_node("Traps").add_child(item_instance)
			Global.remove_item_from_player(item)
		else:
			print("Object on position: " + str(item_position))

func is_object_on_position(position):
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_point(position)
	if len(result) == 1:
		var is_enemy = result[0].collider.is_in_group("enemy")
		var is_player = result[0].collider.name == "Player"
		if is_player or is_enemy:
			return 0

	# print(result)

	return len(result) != 0

func _on_CountdownTimer_timeout():
	if Global.current_map == -1:
		animation_player_fader.play("fade_out_player_won")
	else:
		if current_countdown <= 1:
			get_current_map_node().get_tree().paused = false
			player.set_state_dynamic()
			countdown_timer.one_shot = true
			countdown_timer.stop()
			countdown_ui.visible = false
			game_started = true
		else:
			current_countdown -= 1
			countdown_label.text = str(current_countdown)

func _on_animationPlayerFader_animation_finished(anim_name: String):
	level_music.stop()
	match anim_name:
		"fade_in":
			start_countdown()
		"fade_out_next_level":
			get_current_map_node().get_tree().paused = true
			player.set_state_static()
			Global.reset_player_items()
			setup_map(Global.current_map + 1)
		"fade_out_player_died":
			if Global.player_current_lifes == 0:
				get_tree().change_scene("res://UI/MainMenu.tscn")
			else:
				Global.reset_player_items()
				player.is_dead = false
				get_tree().reload_current_scene()
		"fade_out_player_won":
			get_tree().change_scene("res://UI/MainMenu.tscn")

func _on_Player_player_dying():
	if Global.player_current_lifes <= 1:  # Players life is updated after sound ends...
		game_over_label.visible = true
	else:
		battle_lost_label.visible = true

func exit_visible():
	sound_exit_visible.play()

func check_level_state(layer):
	var level_resolvabel = false

	match layer:
		"air":
			level_resolvabel = Global.has_player_item("dynamite")
		"floor":
			level_resolvabel = Global.has_player_item("dynamite") or Global.has_player_item("bear_trap")

	if not level_resolvabel:
		if Global.difficulty >= 4:
			no_ammo_label.visible = true
			no_ammo_timer.one_shot = false
			no_ammo_timer.start()
		else:
			print("spawning ammo: ", layer)
			spawn_ammo(layer)

func _on_no_ammo_time_timeout():
	_on_Player_player_died()

func spawn_ammo(layer: String):
	get_current_map_node().spawn_ammo(layer)

func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if game_started:
			get_tree().paused = not get_tree().paused
			if get_tree().paused:
				player.set_state_static()
				pause_menu.visible = true
			else:
				player.set_state_dynamic()
				pause_menu.visible = false
