extends Node2D

signal map_finished
signal exit_is_visible
signal map_unresolvable(layer)

export(String) var map_name = "Map"

var exit_visible_state = false
var exit_origin: Vector2 = Vector2.ZERO
var item_chest = preload("res://Ressources/Items/Item chest.tscn")

onready var player_marker = $PlayerMarker

func get_camera_limit_right():
	return get_node("CameraLimit").position.x

func get_camera_limit_bottom():
	return get_node("CameraLimit").position.y

func get_player_position():
	return get_node("PlayerMarker").position

func setup_map(difficulty: int):
	set_difficulty(difficulty)
	connect_enemies()
	exit_visible(false)

func set_difficulty(difficulty: int):
	var enemy_speed_offset = 0

	match difficulty:
		0: enemy_speed_offset = -10
		1: enemy_speed_offset = -10
		4: enemy_speed_offset = 8
		5: enemy_speed_offset = 15

	apply_speed_offset_to_enemies(enemy_speed_offset)

func apply_speed_offset_to_enemies(speed_offset):
	var enemies_node = get_node("Enemies")
	for enemy in enemies_node.get_children():
		enemy.MAX_SPEED += speed_offset

func connect_enemies():
	var enemies_node = get_node("Enemies")
	for enemy in enemies_node.get_children():
		enemy.connect("enemy_died", self, "enemy_died")
		enemy.connect("enemy_hurt", self, "enemy_hurt")

func enemy_died():
	# print("Enemy died event triggered")
	var enemies_alive = get_alive_enemies("enemy") > 0
	# print("Emeny alive: ", enemies_alive)
	check_environment_state()
	if not enemies_alive:
		exit_visible(true)

func enemy_hurt():
	check_environment_state()

func exit_visible(visible: bool):
	var exit_node = get_node("Items/Exit")
	if visible:
		exit_node.position = exit_origin
		emit_signal("exit_is_visible")
	else:
		exit_origin = exit_node.position
		exit_node.position = Vector2(-100, -100)

func check_environment_state():
	var items_left = get_node("Items").get_child_count() - 1
	
	if items_left <= 0:
		var enemies_air = get_alive_enemies("enemy_air")
		var traps_air = get_active_traps("trap_air")
		var enemies_floor = get_alive_enemies("enemy_floor")
		var traps_floor = get_active_traps("trap_floor")
		print("il: ", items_left, " # ea: ", enemies_air, " # ta: ", traps_air, " # ef: ", enemies_floor, " # tf: ", traps_floor)
		if enemies_air > 0 and traps_air <= 0:
			print("unresolvable air")
			emit_signal("map_unresolvable", "air")
		elif enemies_floor > 0 and traps_floor <= 0:
			print("unresolvable floor")
			emit_signal("map_unresolvable", "floor")

func get_active_traps(group):
	var active_traps = 0
	var nodes = get_node("Traps").get_children()
	for node in nodes:
		if node.is_in_group(group):
			if node.has_method("trap_is_active"):
				if node.trap_is_active():
					active_traps += 1
			else:
				active_traps += 1
	
	return active_traps

func get_alive_enemies(group):
	var enemies_alive = 0
	var nodes = get_node("Enemies").get_children()
	for node in nodes:
		if node.is_in_group(group):
			if not node.is_enemy_dead():
				enemies_alive += 1
	
	return enemies_alive

func spawn_ammo(layer: String):
	var item_instance = item_chest.instance()
	item_instance.weapon_type = "bear_trap"
	if layer == "air":
		item_instance.weapon_type = "dynamite"
	item_instance.position = player_marker.position
	
	get_node("Items").call_deferred("add_child", item_instance)

func _on_Exit_player_exited():
	emit_signal("map_finished")
