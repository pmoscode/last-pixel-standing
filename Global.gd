extends Node

#const game_data_path = "res://last-pixel-standing.save"
const game_data_path = "user://last-pixel-standing.save"

var difficulty_options = {
	"ewt": "Easy (with tutorial)",
	"ewot": "Easy (without tutorial)",
	"nwt": "Normal (with tutorial)",
	"nwot": "Normal (without tutorial)",
	"h": "Hard",
	"n": "Nightmare"	
}

var debug_hints = {
	"test_map": false,
	"god_mode": false
}

var difficulty: int = 2 setget set_difficulty, get_difficulty
var player = null setget set_player
var player_items = {}
var player_current_lifes = 3 setget set_player_current_lifes, get_player_current_lifes
var map_count = 13
var preselected_map = -1
var current_map = 0 # Maps 1, 2, 3 are training levels
var game_data = {
	"max_map": 1
}

signal player_item_changed(item, new_value)
signal player_lifes_changed

func set_player(value):
	player = value

func init_player_current_lifes():
	player_current_lifes = get_player_initial_lifes(get_difficulty())

func set_difficulty(value):
	difficulty = value
	
func get_difficulty():
	return difficulty

func get_player_current_lifes():
	return player_current_lifes
	
func set_player_current_lifes(value):
	player_current_lifes = value
	emit_signal("player_lifes_changed")

func add_item_to_player(item):
	if not player_items.has(item):
		player_items[item] = 1
	else:
		player_items[item] += 1
	
	emit_signal("player_item_changed", item, player_items[item])

func update_player_items_in_ui():
	for item in player_items.keys():
		emit_signal("player_item_changed", item, player_items[item])

func remove_item_from_player(item):
	if has_player_item(item):
		player_items[item] -= 1
		emit_signal("player_item_changed", item, player_items[item])

func has_player_item(item):
	return player_items.has(item) and player_items[item] > 0

func reset_player_items():
	if difficulty >= 4 or current_map <=3:
		emit_signal("player_item_changed", "dynamite", 0)
		emit_signal("player_item_changed", "bear_trap", 0)
		player_items = {}

func get_difficulty_of_menu_index(index: int):
	var keys = difficulty_options.keys()
	difficulty = index
	
	return keys[index]

func get_player_initial_lifes(difficulty):
	match difficulty:
		0: return 4
		1: return 4
		2: return 3
		3: return 3
		4: return 2
		5: return 1
		
func is_preselected_map():
	return preselected_map > -1

func save_game_data():
	var save_game_data_file = File.new()
	save_game_data_file.open_encrypted_with_pass(game_data_path, File.WRITE, "pmoscodeLPS")
	save_game_data_file.store_line(to_json(game_data))
	print("Savegame saved: ", game_data, " to ", save_game_data_file.get_path_absolute())	
	save_game_data_file.close()
	
func load_game_data():
	var save_game_data_file = File.new()
	if not save_game_data_file.file_exists(game_data_path):
		print("Savegame doesn't exist. Saving now...")
		save_game_data()
		
	save_game_data_file.open_encrypted_with_pass(game_data_path, File.READ, "pmoscodeLPS")
	game_data = parse_json(save_game_data_file.get_line())
	print("Savegame loded: ", game_data, " from ", save_game_data_file.get_path_absolute())
	save_game_data_file.close()
