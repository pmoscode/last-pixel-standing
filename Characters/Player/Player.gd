extends KinematicBody2D

export var ACCELERATION = 500
export var MAX_SPEED = 100
export var ROLL_SPEED = 120
export var FRICTION = 500

signal player_died
signal player_dying
signal player_dropped_item(item)

onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")
onready var hurtbox = $Hurtbox
onready var hurtbox_collision_shape = $Hurtbox/CollisionShape2D
onready var sprite = $Player
onready var sound = $AudioStreamPlayer2D

var walk_direction = 0
var velocity = Vector2.ZERO
var is_dead = false setget set_is_dead, get_is_dead
var state = STATE.DYNAMIC

enum STATE {
	DYNAMIC,
	STATIC
}

func _ready():
	hurtbox.get_node("CollisionShape2D").disabled = false
	animationTree.active = true
	hurtbox.connect("hurtanimation_finished", self, "player_died_animation_finished")

func _process(delta):
	#if Input.is_action_pressed("ui_cheat_god"):
	#	Global.debug_hints.god_mode = !Global.debug_hints.god_mode
	#	change_god_mode()
	
	#if Input.is_action_just_pressed("ui_cheat_more_ammo"):
	#	self.add_item("bear_trap")
	#	self.add_item("dynamite")
	pass

func _physics_process(delta):

	if is_dead:
		set_state_static()

	match state:
		STATE.DYNAMIC:
			dynamic_state(delta)
		STATE.STATIC:
			static_state()

func dynamic_state(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized()

	check_item_drop()

	if input_vector != Vector2.ZERO:
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Run/blend_position", input_vector)

		animationState.travel("Run")
		walk_direction = -input_vector.x
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)
	else:
		animationState.travel("Idle")
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)

	velocity = move_and_slide(velocity)

func static_state():
	velocity = Vector2.ZERO
	animationState.travel("Idle")

func set_state_dynamic():
	state = STATE.DYNAMIC

func set_state_static():
	state = STATE.STATIC

func set_is_dead(value: bool):
	is_dead = value
	hurtbox.get_node("CollisionShape2D").disabled = value

func get_is_dead():
	return is_dead

func add_item(item: String):
	Global.add_item_to_player(item)

func check_item_drop():
	if Input.is_action_just_pressed("ui_item_1"):
		emit_signal("player_dropped_item", "dynamite")

	if Input.is_action_just_pressed("ui_item_2"):
		emit_signal("player_dropped_item", "bear_trap")

func hurt(_hit_ponts):
	hurtbox.create_hit_effect(sprite.position, walk_direction)
	sound.play()

func player_died_animation_finished():
	self.is_dead = true
	emit_signal("player_dying")

func _on_AudioStreamPlayer2D_finished():
	emit_signal("player_died")

func disable_hurtbox(value: bool):
	hurtbox_collision_shape.disabled = value

func change_god_mode():
	if Global.debug_hints.god_mode:
		disable_hurtbox(true)
	else:
		disable_hurtbox(false)
