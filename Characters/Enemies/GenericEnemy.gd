extends KinematicBody2D

export var ACCELERATION = 300
export var MAX_SPEED = 50
export var FRICTION = 200
export(int) var max_health = 1 

onready var hurtbox = $Hurtbox
onready var hitbox = $Hitbox
onready var enemy_sprite = $Enemy
onready var animated_enemy_sprite = $AnimatedEnemy
onready var sound = $AudioStreamPlayer2D
onready var soft_collision = $SoftCollision
onready var health_indicator = $TopLevelPosition/HealthIndicator
onready var health = max_health

var velocity = Vector2.ZERO
var is_dead = false

signal enemy_died
signal enemy_hurt

func _ready():
	if max_health > 1:
		health_indicator.min_value = 0
		health_indicator.step = 1
		health_indicator.max_value = max_health
		health_indicator.value = health
		health_indicator.visible = true

func _physics_process(delta):
	if Global.player != null and not is_dead:
		var direction = global_position.direction_to(Global.player.global_position)
		velocity = velocity.move_toward(direction * MAX_SPEED, ACCELERATION * delta)

	if is_dead:
		velocity = Vector2.ZERO

	# enemy_sprite.flip_h = velocity.x < 0
	animated_enemy_sprite.flip_h = velocity.x < 0

	if soft_collision.is_colliding():
		velocity += soft_collision.get_push_vector() * delta * 500

	velocity = move_and_slide(velocity)

func hurt(hit_points):
	health -= hit_points
	health_indicator.value = health
	MAX_SPEED -= 20
	ACCELERATION -= 50
	FRICTION -= 50
	if health <= 0:
		is_dead = true
		emit_signal("enemy_died")
		health_indicator.value = 0
		hurtbox.create_hit_effect(enemy_sprite.position, velocity.x)
		animated_enemy_sprite.stop()
		sound.play()
	else:
		emit_signal("enemy_hurt")

func is_enemy_dead():
	return is_dead

func _on_AudioStreamPlayer2D_finished():
	queue_free()

func _on_Hitbox_area_entered(area):
	var parent = area.get_parent()
	if parent.has_method("hurt"):
		parent.hurt(1)
