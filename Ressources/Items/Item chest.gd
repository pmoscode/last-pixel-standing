extends Area2D

export(String, "dynamite", "bear_trap") var weapon_type = "dynamite"

onready var sound = $AudioStreamPlayer2D
onready var sprite = $Sprite

func _on_Item_chest_body_entered(body):
	if body.name == "Player":
		sprite.visible = false
		sound.play()
		body.add_item(weapon_type)

func _on_AudioStreamPlayer2D_finished():
	queue_free()
