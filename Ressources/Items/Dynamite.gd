extends Area2D

export(int) var hit_points = 1

onready var sprite = $Dynamite
onready var animatedSprite = $Explosion
onready var collision_shape = $Hitbox/CollisionShape2D
onready var sound = $AudioStreamPlayer2D

var is_active = true

func _on_Hitbox_area_entered(area):
	if area.get_parent().has_method("hurt"):
		is_active = false
		area.get_parent().hurt(hit_points)
		animatedSprite.visible = true
		if not animatedSprite.is_playing():
			animatedSprite.play("explode")
			sound.play()

func _on_Explosion_animation_finished():
	queue_free()

func _on_Explosion_frame_changed():
	if animatedSprite.frame == 4:
		collision_shape.disabled = true
	
	if animatedSprite.frame == 5:
		sprite.visible = false

func trap_is_active():
	return is_active
