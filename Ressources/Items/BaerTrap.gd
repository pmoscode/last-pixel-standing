extends Area2D

export(int) var hit_points = 1

onready var animation = $AnimatedSprite
onready var hitbox = $Hitbox
onready var sound = $AudioStreamPlayer2D

var is_active = true

func _on_Hitbox_area_entered(area):	
	if not area.get_parent().is_in_group("enemy_air"):
		if area.get_parent().has_method("hurt"):
			is_active = false
			area.get_parent().hurt(hit_points)
			destroy()

func destroy():
	animation.play("action")
	sound.play()
	call_deferred("remove_child", hitbox)

func trap_is_active():
	return is_active

func _on_AnimatedSprite_animation_finished():
	animation.stop()
	animation.frame = 3
