extends Area2D

export(int) var hit_points = 1

onready var animatedSprite = $AnimatedSprite
onready var sound = $AudioStreamPlayer2D

func _on_AnimatedSprite_animation_finished():
	animatedSprite.stop()
	animatedSprite.frame = 0

func _on_Peaks_body_entered(_body):
	animatedSprite.play("hurt")
	sound.play()

func _on_Hitbox_area_entered(area):
	if area.get_parent().has_method("hurt") and not area.get_parent().is_in_group("enemy_air"):
		area.get_parent().hurt(hit_points)	
		if animatedSprite.frame == 5:
			animatedSprite.frame = 0
		animatedSprite.play("hurt")
		sound.play()
