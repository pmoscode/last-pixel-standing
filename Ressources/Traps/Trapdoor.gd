extends Area2D

export(int) var hit_points = 10
export(int, 1, 4) var bodies_need_to_release = 2

onready var animatedSprite = $AnimatedSprite
onready var sound = $AudioStreamPlayer2D

var bodies_on_trap = 0
var state = "trap" # or "pit"

func is_enemy_floor_or_player(body):
	var is_enemy_floor = body.is_in_group("enemy_floor")
	var is_player = body.is_in_group("player")

	return is_enemy_floor or is_player

func _on_Hitbox_area_entered(area):
	var has_hurt_method = area.get_parent().has_method("hurt")
	var is_enemy_floor_or_player = is_enemy_floor_or_player(area.get_parent())

	if has_hurt_method and is_enemy_floor_or_player:
		if state == "trap":
			if bodies_on_trap >= bodies_need_to_release:
				print("releasing trap: bodies = ", bodies_on_trap)
				area.get_parent().hurt(hit_points)
				animatedSprite.play("action")
				sound.play()
				state = "pit"
				bodies_on_trap = 0
			else:
				print("Not enough bodies on trap: ", bodies_on_trap, " -- needs: ", bodies_need_to_release)
		else:
			area.get_parent().hurt(hit_points)

func _on_AnimatedSprite_animation_finished():
	animatedSprite.stop()
	sound.stream_paused = true

func _on_Trapdoor_body_entered(body):
	if is_enemy_floor_or_player(body) and state == "trap":
		bodies_on_trap += 1

func _on_Trapdoor_body_exited(body):
	if is_enemy_floor_or_player(body) and state == "trap":
		bodies_on_trap -= 1
