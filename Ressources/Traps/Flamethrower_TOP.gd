extends Area2D

export(int) var hit_points = 1

onready var animatedSprite = $Stream
onready var sound = $AudioStreamPlayer2D

func _on_Stream_animation_finished():
	animatedSprite.stop()
	animatedSprite.frame = 0

func _on_Flamethrower_TOP_body_entered(_body):
	animatedSprite.play("fire")
	sound.play()

func _on_Hitbox_area_entered(area):
	if area.get_parent().has_method("hurt"):
		area.get_parent().hurt(hit_points)
		if animatedSprite.frame == 2:
			animatedSprite.frame = 0
		animatedSprite.play("fire")
		sound.play()
