extends Area2D

signal player_exited

func _on_Exit_body_entered(body):
	print(body.name)
	if body.is_in_group("player"):
		emit_signal("player_exited")
