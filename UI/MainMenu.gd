extends Control

onready var difficulty_dialog = $DifficultyDialog
onready var how_to_play_dialog = $HowToPlayDialog
onready var credits_dialog = $CreditsDialog
onready var difficulty_label = $MarginContainer/VBoxContainer/VBoxContainer/Difficulty
onready var difficulty_settings = $DifficultyDialog/ItemList
onready var map_select = $MapSelect

func _ready():
	difficulty_settings.add_item(Global.difficulty_options.ewt)
	difficulty_settings.add_item(Global.difficulty_options.ewot)
	difficulty_settings.add_item(Global.difficulty_options.nwt)
	difficulty_settings.add_item(Global.difficulty_options.nwot)
	difficulty_settings.add_item(Global.difficulty_options.h)
	difficulty_settings.add_item(Global.difficulty_options.n)
	
	change_difficulty_button_text()
	Global.load_game_data()
	setup_select_map()
	
func change_difficulty_button_text():
	var difficulty = Global.difficulty
	var difficulty_key = Global.get_difficulty_of_menu_index(difficulty)
	
	var text = "Difficulty (" + Global.difficulty_options[difficulty_key] + ")"
	difficulty_label.text = text

func setup_select_map():
	var max_map = Global.game_data.max_map
	if max_map > 3:
		var grid_container = map_select.get_node("GridContainer")
		for map_node in grid_container.get_children():
			var map_number = int(map_node.name.split("_")[1])
			if map_number <= max_map:
				map_node.disabled = false
				map_node.connect("pressed", self, "map_select_button_pressed", [map_number])
	
func _on_StartGame_pressed():
	if Input.is_action_pressed("ui_cheat_test_map"):
		Global.debug_hints.test_map = true
	
	Global.init_player_current_lifes()
	Global.current_map = 0
	get_tree().change_scene("res://Game/Game.tscn")

func _on_Difficulty_pressed():
	difficulty_dialog.popup()

func _on_Exit_pressed():
	get_tree().quit()

func _on_ItemList_item_selected(index):
	if difficulty_settings.is_item_selectable(index):
		Global.difficulty = index
		difficulty_dialog.hide()
		change_difficulty_button_text()

func _on_HowToPlay_pressed():
	how_to_play_dialog.popup()

func _on_SelectMap_pressed():
	map_select.visible = true

func _on_CloseMapSelect_pressed():
	map_select.visible = false

func map_select_button_pressed(selected_map):
	Global.preselected_map = selected_map
	_on_StartGame_pressed()

func _on_Credits_pressed():
	credits_dialog.popup()
