extends Control

var player = null

func _ready():
	player = get_node("../../Player")

func _on_Continue_pressed():
	get_tree().paused = false
	player.set_state_dynamic()
	visible = false

func _on_Back2Menu_pressed():
	get_tree().change_scene("res://UI/MainMenu.tscn")

func _on_Back2OS_pressed():
	get_tree().quit()
