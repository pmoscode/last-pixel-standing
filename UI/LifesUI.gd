extends TextureRect

onready var full_life = $LifesFull

func _ready():
	update_player_life()
	Global.connect("player_lifes_changed", self, "update_player_life")

func update_player_life():
	full_life.rect_size.x = Global.player_current_lifes * 15
