extends Control

onready var dynamite_count = $TextureRect/HBoxContainer/DynamiteCount
onready var beartrap_count = $TextureRect/HBoxContainer/BaerTrapCount

func _ready():
	Global.connect("player_item_changed", self, "set_ui_items")

func set_ui_items(item, new_value):
	match item:
		"dynamite":
			dynamite_count.text = str(new_value)
		"bear_trap":
			beartrap_count.text = str(new_value)
